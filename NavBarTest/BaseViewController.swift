//
//  BaseViewController.swift
//  NavBarTest
//
//  Created by Dima Panychyk on 12/1/16.
//  Copyright © 2016 Dima Panychyk. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
  
  var lastSnapshot: UIImage? = nil
  let backgroundImageView = UIImageView()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Transparent navigation bar
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.navigationController?.navigationBar.isTranslucent = true
    
    lastSnapshot = UIImage(named: "london_iphone_home")
//    1 way
//    self.view.backgroundColor = UIColor.init(patternImage: lastSnapshot!)
    
//    2 way
    backgroundImageView.backgroundColor = .clear
    backgroundImageView.contentMode = .scaleAspectFill
    backgroundImageView.image = lastSnapshot
    self.view.insertSubview(backgroundImageView, at: 0)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    //_dimmingView.frame = self.view.bounds;
    self.backgroundImageView.frame = self.view.bounds;
    //CGRect loadingWheelFrame = UIEdgeInsetsInsetRect(_dimmingView.bounds, self.loadingWheelInsets);
    //_loadingWheel.frame = loadingWheelFrame;
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  public func setImage(image: UIImage) {
    
  }
  
}
