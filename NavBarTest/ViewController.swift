//
//  ViewController.swift
//  NavBarTest
//
//  Created by Dima Panychyk on 11/30/16.
//  Copyright © 2016 Dima Panychyk. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func nextViewCobtroller(_ sender: UIButton) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let someVC = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
    self.navigationController?.pushViewController(someVC, animated: true)
  }
  

}
