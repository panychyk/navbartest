//
//  TransitionAnimationController.swift
//  NavBarTest
//
//  Created by Dima Panychyk on 12/1/16.
//  Copyright © 2016 Dima Panychyk. All rights reserved.
//

import UIKit

class TransitionAnimationController: NSObject {
  
  var navigationOperation: UINavigationControllerOperation!
  
  weak var _transitioningView: UIView!
  weak var _transitioningController: BaseViewController!
  
}

extension TransitionAnimationController: UIViewControllerAnimatedTransitioning {
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.25 // kAnimationDuration
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    
    let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
    let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
    
    let containerView = transitionContext.containerView
    
    if self.navigationOperation == .push {
      //Add 'to' view to the hierarchy with 0.0 scale
      _transitioningView = toViewController?.view;
      _transitioningController = toViewController as! BaseViewController
      _transitioningController.view.frame = containerView.bounds;
      toViewController?.view.transform = CGAffineTransform(translationX: containerView.frame.size.width, y: 0);
      containerView.insertSubview((toViewController?.view)!, aboveSubview: (fromViewController?.view)!)
      
      UIView.animate(withDuration: 0.25, animations: { [weak toViewController] in
        toViewController?.view.transform = CGAffineTransform(translationX: 0,y: 0);
      }, completion: { (Bool) in
        transitionContext.completeTransition(true)
      })
      
    } else if self.navigationOperation == .pop {
      fromViewController?.view.transform = CGAffineTransform.identity

      containerView.insertSubview((toViewController?.view)!, belowSubview: (fromViewController?.view)!)

      UIView.animate(withDuration: 0.25, animations: {
        UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
        fromViewController?.view.transform = CGAffineTransform(translationX: containerView.frame.width, y: 0);
      }, completion: { (Bool) in
        transitionContext.completeTransition(true)
      })

    }
  }
}
