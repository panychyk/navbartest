//
//  NavigationController.swift
//  NavBarTest
//
//  Created by Dima Panychyk on 11/30/16.
//  Copyright © 2016 Dima Panychyk. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

  let transitionAnimationController = TransitionAnimationController()
  
    override func viewDidLoad() {
        super.viewDidLoad()
      self.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension NavigationController: UINavigationControllerDelegate {
  
  func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return nil
  }
  
  func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    
    let transitionAnimationController = TransitionAnimationController()
    if operation == .push {
      transitionAnimationController.navigationOperation = operation
      return transitionAnimationController
    } else if operation == .pop {
      transitionAnimationController.navigationOperation = operation
      return transitionAnimationController
    } else {
      return nil
    }
  }
  
  func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    
    let viewControllersCount = navigationController.viewControllers.count
    let previousViewController: UIViewController? = navigationController.viewControllers[viewControllersCount - 1]
    
    if ((previousViewController != nil) && ((viewController.isKind(of: BaseViewController.self)) ||
      viewController.responds(to: #selector(BaseViewController.setImage(image:))))) {
        let vc = viewController as! BaseViewController
      vc.setImage(image: vc.lastSnapshot!)
    }
  }
  
}
